#!/bin/bash
git clone --filter=blob:none --no-checkout --single-branch --branch master https://github.com/ventoy/Ventoy.git /tmp/ventoy-source
cd /tmp/ventoy-source

# Get the list of Git commits
COMMITS=$(git log --pretty=format:"%h %ai %s" HEAD)

# Create an array of the commit messages
COMMIT_MSGS=($COMMITS)

# Write the changelog to a file
echo -e "%changelog\n" > changelog.txt

# Iterate over the commit messages and add each one to the changelog
for COMMIT_MSG in "${COMMIT_MSGS[@]}"; do
  COMMIT_HASH=$(echo $COMMIT_MSG | awk '{print $1}')
  COMMIT_DATE=$(echo $COMMIT_MSG | awk '{print $2}')
  COMMIT_SUMMARY=$(echo $COMMIT_MSG | awk '{$1=$2=""; print $0}')

  echo -e "* $COMMIT_DATE $COMMIT_HASH\n- $COMMIT_SUMMARY\n" >> /tmp/ventoy-change.log
done
