#!/bin/bash

# Add build user
if id -u "builder" >/dev/null 2>&1; then
  echo "user exists, skip creating";
else
  echo "builder user does not exist, creating";
  useradd -m -c "User for building stuff." -g users builder;
fi

# Add COPR token
sudo -u builder mkdir -p /home/builder/.config;
sudo -u builder touch /home/builder/.config/copr;
printf '%s\n' '[copr-cli]' 'login = ${CI_CUSTOM_COPR_LOGIN}' 'username = karlisk' 'token = ${CI_CUSTOM_COPR_TOKEN}' 'copr_url = https://copr.fedorainfracloud.org' '# expiration date: ${CI_CUSTOM_COPR_TOKEN_EXPIRATION}' >> /home/builder/.config/copr;

# Check that Copr token isn't expired
# '$CI_CUSTOM_COPR_TOKEN_EXPIRATION' is set via GitLab CI variable
export CI_CUSTOM_COPR_TOKEN_EXPIRATION="2024-04-26"
if [[ $(date +%s) -gt $(date -d "$CI_CUSTOM_COPR_TOKEN_EXPIRATION" +%s) ]]; then
  echo "Copr token expired! $CI_CUSTOM_COPR_TOKEN_EXPIRATION";
  exit 1;
else
  echo "Copr token valid!";
fi

# Remove COPR token config to avoid exposing secrets
rm -rf /home/builder/.config/copr;

dnf makecache; # Populate DNF repo info
dnf groupinstall -y "Development Tools" "RPM Development Tools"; # Install build prequisites
dnf install -y rpmlint copr-cli; # Install linting and Copr push prequisites
dnf clean all; # Post-install cleanup

exit 0;
