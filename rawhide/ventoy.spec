%global release_url https://github.com/ventoy/Ventoy
%global ventoy_checksum 9a0d5dcd74442213bd2eca0211abae051f444861ccdd1be814dfb9bc75341c30

Name:			ventoy
Version:		1.0.88
Release:		2%{?dist}
Summary:		"Ventoy is an open source tool to create bootable USB drive for ISO/WIM/IMG/VHD(x)/EFI files."

License:		GPLv3+
URL:			https://www.ventoy.net/
Source0:		%{release_url}/releases/download/v%{version}/%{name}-%{version}-linux.tar.gz
Source1:		%{release_url}/raw/master/ICON/logo_256.png
Source2:		change.log

BuildRequires:	curl tar coreutils desktop-file-utils
Requires:		xz

Provides:		mount.exfat-fuse.xz = %{version}-%{release}
Provides:		hexdump.xz = %{version}-%{release}
Provides:		ash = %{version}-%{release}
Provides:		Ventoy2Disk.gtk2 = %{version}-%{release}
Provides:		V2DServer.xz = %{version}-%{release}
Provides:		vtoycli.xz = %{version}-%{release}
Provides:		Ventoy2Disk.qt5 = %{version}-%{release}
Provides:		mkexfatfs.xz = %{version}-%{release}
Provides:		Plugson = %{version}-%{release}
Provides:		Ventoy2Disk.gtk3 = %{version}-%{release}
Provides:		vlnk = %{version}-%{release}
Provides:		xzcat = %{version}-%{release}

%ifarch x86_64
Provides:		VentoyGUI.x86_64 = %{version}-%{release}
%endif
%ifarch aarch64
Provides:		VentoyGUI.aarch64 = %{version}-%{release}
%endif
%ifarch mips64el
Provides:		VentoyGUI.mips64el = %{version}-%{release}
%endif

Provides:		ventoy.desktop = %{version}-%{release}
Provides:		ventoy-gui = %{version}-%{release}
Provides:		ventoy.disk.img = %{version}-%{release}
Provides:		VentoyVlnk.sh = %{version}-%{release}
Provides:		VentoyWeb.sh = %{version}-%{release}
Provides:		VentoyGTK.glade = %{version}-%{release}
Provides:		ENROLL_THIS_KEY_IN_MOKMANAGER.cer = %{version}-%{release}
Provides:		VentoyWorker.sh = %{version}-%{release}
Provides:		plugson.tar = %{version}-%{release}
Provides:		distro_gui_type.json = %{version}-%{release}
Provides:		ventoy_lib.sh = %{version}-%{release}
Provides:		languages.json = %{version}-%{release}
Provides:		font-awesome.min.css = %{version}-%{release}
Provides:		ionicons.min.css = %{version}-%{release}
Provides:		vtoy.css = %{version}-%{release}
Provides:		VentoyLogo.png = %{version}-%{release}
Provides:		dropdown.png = %{version}-%{release}
Provides:		refresh.ico = %{version}-%{release}
Provides:		vtoy.js = %{version}-%{release}
Provides:		jQuery-2.1.4.min.js = %{version}-%{release}
Provides:		jquery.validate.min.js = %{version}-%{release}
Provides:		jquery.vtoy.alert.js = %{version}-%{release}
Provides:		languages.js = %{version}-%{release}
Provides:		bootstrap.min.css = %{version}-%{release}
Provides:		bootstrap-theme.min.css = %{version}-%{release}
Provides:		bootstrap.min.js = %{version}-%{release}
Provides:		glyphicons-halflings-regular.woff2 = %{version}-%{release}
Provides:		glyphicons-halflings-regular.ttf = %{version}-%{release}
Provides:		glyphicons-halflings-regular.eot = %{version}-%{release}
Provides:		glyphicons-halflings-regular.woff = %{version}-%{release}
Provides:		glyphicons-halflings-regular.svg = %{version}-%{release}
Provides:		fontawesome-webfont.ttf = %{version}-%{release}
Provides:		fontawesome-webfont.woff2 = %{version}-%{release}
Provides:		glyphicons-halflings-regular.woff2 = %{version}-%{release}
Provides:		glyphicons-halflings-regular.ttf = %{version}-%{release}
Provides:		ionicons.eot = %{version}-%{release}
Provides:		glyphicons-halflings-regular.woff = %{version}-%{release}
Provides:		ionicons.ttf = %{version}-%{release}
Provides:		fontawesome-webfont.woff = %{version}-%{release}
Provides:		skin-blue.min.css = %{version}-%{release}
Provides:		AdminLTE.min.css = %{version}-%{release}
Provides:		app.min.js = %{version}-%{release}
Provides:		favicon.ico = %{version}-%{release}
Provides:		index.html = %{version}-%{release}
Provides:		ExtendPersistentImg.sh = %{version}-%{release}
Provides:		Ventoy2Disk.sh = %{version}-%{release}
Provides:		VentoyPlugson.sh = %{version}-%{release}
Provides:		ventoy_grub.cfg = %{version}-%{release}
Provides:		menu_n.png = %{version}-%{release}
Provides:		menu_ne.png = %{version}-%{release}
Provides:		slider_c.png = %{version}-%{release}
Provides:		select_c.png = %{version}-%{release}
Provides:		terminal_box_s.png = %{version}-%{release}
Provides:		slider_s.png = %{version}-%{release}
Provides:		menu_sw.png = %{version}-%{release}
Provides:		terminal_box_c.png = %{version}-%{release}
Provides:		menu_w.png = %{version}-%{release}
Provides:		terminal_box_ne.png = %{version}-%{release}
Provides:		terminal_box_w.png = %{version}-%{release}
Provides:		terminal_box_e.png = %{version}-%{release}
Provides:		slider_n.png = %{version}-%{release}
Provides:		terminal_box_nw.png = %{version}-%{release}
Provides:		menu_e.png = %{version}-%{release}
Provides:		terminal_box_sw.png = %{version}-%{release}
Provides:		menu_c.png = %{version}-%{release}
Provides:		ubuntu.png = %{version}-%{release}
Provides:		deepin.png = %{version}-%{release}
Provides:		vtoyiso.png = %{version}-%{release}
Provides:		red-hat.png = %{version}-%{release}
Provides:		background.png = %{version}-%{release}
Provides:		menu_s.png = %{version}-%{release}
Provides:		terminal_box_n.png = %{version}-%{release}
Provides:		terminal_box_se.png = %{version}-%{release}
Provides:		menu_nw.png = %{version}-%{release}
Provides:		menu_se.png = %{version}-%{release}
Provides:		theme.txt = %{version}-%{release}
Provides:		ventoy.json = %{version}-%{release}
Provides:		CreatePersistentImg.sh = %{version}-%{release}
Provides:		boot.img = %{version}-%{release}
Provides:		core.img = %{version}-%{release}

%description
Ventoy can be installed on a USB flash drive, local disk, SSD (NVMe), or SD Card and it will directly boot from the selected .iso, .wim, .img, .vhd(x), or .efi file(s) added. Ventoy does not extract the image file(s) to the USB drive, but uses them directly. It is possible to place multiple ISO images on a single device and select the image to boot from the menu displayed just after Ventoy boots. MBR and GPT partition styles, x86 Legacy BIOS and various UEFI boot methods (including persistence) are supported. ISO files larger than 4 GB can be used. Ventoy supports various operating system boot and installation ISO files including Windows 7 and above, Debian, Ubuntu, CentOS, RHEL, Deepin, Fedora and more than a hundred of other Linux distributions, various UNIX releases, VMware, Citrix XenServer, etc.

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} %{release_url}/releases/download/v%{version}/%{name}-%{version}-linux.tar.gz

# Get the Application icon
curl --location --remote-time --show-error --fail --output %{SOURCE1} %{release_url}/raw/master/ICON/logo_256.png

# Generate the Ventoy Changelog to /tmp/ventoy-change.log
./%{SOURCE2}

# Generate .desktop entry file
%ifarch x86_64
	printf '%s\n' '[Desktop Entry]' 'Type=Application' 'Name=Ventoy GUI' 'Comment=%{summary}' 'Icon=/usr/share/icons/ventoy.png' 'Exec=/opt/ventoy/VentoyGUI.x86_64' 'Terminal=false' 'X-Categories=Utilities;' 'Keywords=usb;iso;bootable;' > ventoy.desktop
%endif
%ifarch aarch64
	printf '%s\n' '[Desktop Entry]' 'Type=Application' 'Name=Ventoy GUI' 'Comment=%{summary}' 'Icon=/usr/share/icons/ventoy.png' 'Exec=/opt/ventoy/VentoyGUI.aarch64' 'Terminal=false' 'X-Categories=Utilities;' 'Keywords=usb;iso;bootable;' > ventoy.desktop
%endif
%ifarch mips64el
	printf '%s\n' '[Desktop Entry]' 'Type=Application' 'Name=Ventoy GUI' 'Comment=%{summary}' 'Icon=/usr/share/icons/ventoy.png' 'Exec=/opt/ventoy/VentoyGUI.mips64el' 'Terminal=false' 'X-Categories=Utilities;' 'Keywords=usb;iso;bootable;' > ventoy.desktop
%endif

if [[ "%{ventoy_checksum}" != 0 ]]; then
	echo "%{ventoy_checksum} %{SOURCE0}" | sha256sum -c
	if [[ $? -eq 0 ]]; then
%setup -q -c -n %{name}-%{version}
	else
		echo "%{ventoy_checksum} %{SOURCE0}" | sha256sum -c
		exit 1
	fi
else
	echo "%{ventoy_checksum} %{SOURCE0}" | sha256sum -c
	exit 1
fi

%install
# Install Application icon
mkdir -p %{buildroot}/usr/share/icons
install -D -m 755 %{SOURCE1} %{buildroot}/usr/share/icons/ventoy.png

# Install Application menuentry
mkdir -p %{buildroot}/usr/share/applications
install -D -m 755 ../ventoy.desktop %{buildroot}/usr/share/applications/ventoy.desktop
desktop-file-install %{buildroot}/usr/share/applications/ventoy.desktop

mkdir -p %{buildroot}/opt/ventoy
	# Fedora supported Arches: https://fedoraproject.org/wiki/Architectures
	%ifarch x86_64
		install -D -m 755 %{name}-%{version}/tool/x86_64/mount.exfat-fuse.xz %{buildroot}/opt/ventoy/tool/x86_64/mount.exfat-fuse.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/hexdump.xz %{buildroot}/opt/ventoy/tool/x86_64/hexdump.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/ash.xz %{buildroot}/opt/ventoy/tool/x86_64/ash.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/Ventoy2Disk.gtk2 %{buildroot}/opt/ventoy/tool/x86_64/Ventoy2Disk.gtk2
		install -D -m 755 %{name}-%{version}/tool/x86_64/V2DServer.xz %{buildroot}/opt/ventoy/tool/x86_64/V2DServer.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/vtoycli.xz %{buildroot}/opt/ventoy/tool/x86_64/vtoycli.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/Ventoy2Disk.qt5 %{buildroot}/opt/ventoy/tool/x86_64/Ventoy2Disk.qt5
		install -D -m 755 %{name}-%{version}/tool/x86_64/mkexfatfs.xz %{buildroot}/opt/ventoy/tool/x86_64/mkexfatfs.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/Plugson.xz %{buildroot}/opt/ventoy/tool/x86_64/Plugson.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/Ventoy2Disk.gtk3 %{buildroot}/opt/ventoy/tool/x86_64/Ventoy2Disk.gtk3
		install -D -m 755 %{name}-%{version}/tool/x86_64/vlnk.xz %{buildroot}/opt/ventoy/tool/x86_64/vlnk.xz
		install -D -m 755 %{name}-%{version}/tool/x86_64/xzcat %{buildroot}/opt/ventoy/tool/x86_64/xzcat
		install -D -m 777 %{name}-%{version}/VentoyGUI.x86_64 %{buildroot}/opt/ventoy/VentoyGUI.x86_64
	%endif
	%ifarch aarch64
		install -D -m 755 %{name}-%{version}/tool/aarch64/mount.exfat-fuse.xz %{buildroot}/opt/ventoy/tool/aarch64/mount.exfat-fuse.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/hexdump.xz %{buildroot}/opt/ventoy/tool/aarch64/hexdump.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/ash.xz %{buildroot}/opt/ventoy/tool/aarch64/ash.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/V2DServer.xz %{buildroot}/opt/ventoy/tool/aarch64/V2DServer.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/vtoycli.xz %{buildroot}/opt/ventoy/tool/aarch64/vtoycli.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/Ventoy2Disk.qt5 %{buildroot}/opt/ventoy/tool/aarch64/Ventoy2Disk.qt5
		install -D -m 755 %{name}-%{version}/tool/aarch64/mkexfatfs.xz %{buildroot}/opt/ventoy/tool/aarch64/mkexfatfs.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/Plugson.xz %{buildroot}/opt/ventoy/tool/aarch64/Plugson.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/Ventoy2Disk.gtk3 %{buildroot}/opt/ventoy/tool/aarch64/Ventoy2Disk.gtk3
		install -D -m 755 %{name}-%{version}/tool/aarch64/vlnk.xz %{buildroot}/opt/ventoy/tool/aarch64/vlnk.xz
		install -D -m 755 %{name}-%{version}/tool/aarch64/xzcat %{buildroot}/opt/ventoy/tool/aarch64/xzcat
		install -D -m 777 %{name}-%{version}/VentoyGUI.aarch64 %{buildroot}/opt/ventoy/VentoyGUI.aarch64
	%endif
	%ifarch mips64el
		install -D -m 755 %{name}-%{version}/tool/mips64el/mount.exfat-fuse.xz %{buildroot}/opt/ventoy/tool/mips64el/mount.exfat-fuse.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/hexdump.xz %{buildroot}/opt/ventoy/tool/mips64el/hexdump.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/ash.xz %{buildroot}/opt/ventoy/tool/mips64el/ash.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/V2DServer.xz %{buildroot}/opt/ventoy/tool/mips64el/V2DServer.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/vtoycli.xz %{buildroot}/opt/ventoy/tool/mips64el/vtoycli.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/Ventoy2Disk.qt5 %{buildroot}/opt/ventoy/tool/mips64el/Ventoy2Disk.qt5
		install -D -m 755 %{name}-%{version}/tool/mips64el/mkexfatfs.xz %{buildroot}/opt/ventoy/tool/mips64el/mkexfatfs.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/Plugson.xz %{buildroot}/opt/ventoy/tool/mips64el/Plugson.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/Ventoy2Disk.gtk3 %{buildroot}/opt/ventoy/tool/mips64el/Ventoy2Disk.gtk3
		install -D -m 755 %{name}-%{version}/tool/mips64el/vlnk.xz %{buildroot}/opt/ventoy/tool/mips64el/vlnk.xz
		install -D -m 755 %{name}-%{version}/tool/mips64el/xzcat %{buildroot}/opt/ventoy/tool/mips64el/xzcat
		install -D -m 777 %{name}-%{version}/VentoyGUI.mips64el %{buildroot}/opt/ventoy/VentoyGUI.mips64el
	%endif
install -D -m 755 %{name}-%{version}/ventoy/ventoy.disk.img.xz %{buildroot}/opt/ventoy/ventoy/ventoy.disk.img.xz
install -D -m 755 %{name}-%{version}/ventoy/version %{buildroot}/opt/ventoy/ventoy/version
install -D -m 755 %{name}-%{version}/VentoyVlnk.sh %{buildroot}/opt/ventoy/VentoyVlnk.sh
install -D -m 755 %{name}-%{version}/VentoyWeb.sh %{buildroot}/opt/ventoy/VentoyWeb.sh
install -D -m 755 %{name}-%{version}/tool/VentoyGTK.glade %{buildroot}/opt/ventoy/tool/VentoyGTK.glade
install -D -m 755 %{name}-%{version}/tool/ENROLL_THIS_KEY_IN_MOKMANAGER.cer %{buildroot}/opt/ventoy/tool/ENROLL_THIS_KEY_IN_MOKMANAGER.cer
install -D -m 755 %{name}-%{version}/tool/VentoyWorker.sh %{buildroot}/opt/ventoy/tool/VentoyWorker.sh
install -D -m 755 %{name}-%{version}/tool/plugson.tar.xz %{buildroot}/opt/ventoy/tool/plugson.tar.xz
install -D -m 755 %{name}-%{version}/tool/distro_gui_type.json %{buildroot}/opt/ventoy/tool/distro_gui_type.json
install -D -m 755 %{name}-%{version}/tool/ventoy_lib.sh %{buildroot}/opt/ventoy/tool/ventoy_lib.sh
install -D -m 755 %{name}-%{version}/tool/languages.json %{buildroot}/opt/ventoy/tool/languages.json
install -D -m 755 %{name}-%{version}/WebUI/static/css/font-awesome.min.css %{buildroot}/opt/ventoy/WebUI/static/css/font-awesome.min.css
install -D -m 755 %{name}-%{version}/WebUI/static/css/ionicons.min.css %{buildroot}/opt/ventoy/WebUI/static/css/ionicons.min.css
install -D -m 755 %{name}-%{version}/WebUI/static/css/vtoy.css %{buildroot}/opt/ventoy/WebUI/static/css/vtoy.css
install -D -m 755 %{name}-%{version}/WebUI/static/img/VentoyLogo.png %{buildroot}/opt/ventoy/WebUI/static/img/VentoyLogo.png
install -D -m 755 %{name}-%{version}/WebUI/static/img/dropdown.png %{buildroot}/opt/ventoy/WebUI/static/img/dropdown.png
install -D -m 755 %{name}-%{version}/WebUI/static/img/refresh.ico %{buildroot}/opt/ventoy/WebUI/static/img/refresh.ico
install -D -m 755 %{name}-%{version}/WebUI/static/js/vtoy.js %{buildroot}/opt/ventoy/WebUI/static/js/vtoy.js
install -D -m 755 %{name}-%{version}/WebUI/static/js/jQuery-2.1.4.min.js %{buildroot}/opt/ventoy/WebUI/static/js/jQuery-2.1.4.min.js
install -D -m 755 %{name}-%{version}/WebUI/static/js/jquery.validate.min.js %{buildroot}/opt/ventoy/WebUI/static/js/jquery.validate.min.js
install -D -m 755 %{name}-%{version}/WebUI/static/js/jquery.vtoy.alert.js %{buildroot}/opt/ventoy/WebUI/static/js/jquery.vtoy.alert.js
install -D -m 755 %{name}-%{version}/WebUI/static/js/languages.js %{buildroot}/opt/ventoy/WebUI/static/js/languages.js
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/css/bootstrap.min.css %{buildroot}/opt/ventoy/WebUI/static/bootstrap/css/bootstrap.min.css
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/css/bootstrap-theme.min.css %{buildroot}/opt/ventoy/WebUI/static/bootstrap/css/bootstrap-theme.min.css
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/js/bootstrap.min.js %{buildroot}/opt/ventoy/WebUI/static/bootstrap/js/bootstrap.min.js
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.woff2 %{buildroot}/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.woff2
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.ttf %{buildroot}/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.ttf
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.eot %{buildroot}/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.eot
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.woff %{buildroot}/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.woff
install -D -m 755 %{name}-%{version}/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.svg %{buildroot}/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.svg
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/fontawesome-webfont.ttf %{buildroot}/opt/ventoy/WebUI/static/fonts/fontawesome-webfont.ttf
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/fontawesome-webfont.woff2 %{buildroot}/opt/ventoy/WebUI/static/fonts/fontawesome-webfont.woff2
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/glyphicons-halflings-regular.woff2 %{buildroot}/opt/ventoy/WebUI/static/fonts/glyphicons-halflings-regular.woff2
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/glyphicons-halflings-regular.ttf %{buildroot}/opt/ventoy/WebUI/static/fonts/glyphicons-halflings-regular.ttf
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/ionicons.eot %{buildroot}/opt/ventoy/WebUI/static/fonts/ionicons.eot
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/glyphicons-halflings-regular.woff %{buildroot}/opt/ventoy/WebUI/static/fonts/glyphicons-halflings-regular.woff
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/ionicons.ttf %{buildroot}/opt/ventoy/WebUI/static/fonts/ionicons.ttf
install -D -m 755 %{name}-%{version}/WebUI/static/fonts/fontawesome-webfont.woff %{buildroot}/opt/ventoy/WebUI/static/fonts/fontawesome-webfont.woff
install -D -m 755 %{name}-%{version}/WebUI/static/AdminLTE/css/skins/skin-blue.min.css %{buildroot}/opt/ventoy/WebUI/static/AdminLTE/css/skins/skin-blue.min.css
install -D -m 755 %{name}-%{version}/WebUI/static/AdminLTE/css/AdminLTE.min.css %{buildroot}/opt/ventoy/WebUI/static/AdminLTE/css/AdminLTE.min.css
install -D -m 755 %{name}-%{version}/WebUI/static/AdminLTE/js/app.min.js %{buildroot}/opt/ventoy/WebUI/static/AdminLTE/js/app.min.js
install -D -m 755 %{name}-%{version}/WebUI/favicon.ico %{buildroot}/opt/ventoy/WebUI/favicon.ico
install -D -m 755 %{name}-%{version}/WebUI/index.html %{buildroot}/opt/ventoy/WebUI/index.html
install -D -m 755 %{name}-%{version}/ExtendPersistentImg.sh %{buildroot}/opt/ventoy/ExtendPersistentImg.sh
install -D -m 755 %{name}-%{version}/Ventoy2Disk.sh %{buildroot}/opt/ventoy/Ventoy2Disk.sh
install -D -m 755 %{name}-%{version}/VentoyPlugson.sh %{buildroot}/opt/ventoy/VentoyPlugson.sh
install -D -m 755 %{name}-%{version}/README %{buildroot}/opt/ventoy/README
install -D -m 755 %{name}-%{version}/plugin/ventoy/ventoy_grub.cfg %{buildroot}/opt/ventoy/plugin/ventoy/ventoy_grub.cfg
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_n.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_n.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_ne.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_ne.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/slider_c.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/slider_c.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/select_c.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/select_c.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_s.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_s.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/slider_s.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/slider_s.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_sw.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_sw.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_c.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_c.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_w.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_w.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_ne.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_ne.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_w.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_w.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_e.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_e.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/slider_n.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/slider_n.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_nw.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_nw.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_e.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_e.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_sw.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_sw.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_c.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_c.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/icons/ubuntu.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/icons/ubuntu.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/icons/deepin.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/icons/deepin.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/icons/vtoyiso.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/icons/vtoyiso.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/icons/red-hat.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/icons/red-hat.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/background.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/background.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_s.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_s.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_n.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_n.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/terminal_box_se.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/terminal_box_se.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_nw.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_nw.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/menu_se.png %{buildroot}/opt/ventoy/plugin/ventoy/theme/menu_se.png
install -D -m 755 %{name}-%{version}/plugin/ventoy/theme/theme.txt %{buildroot}/opt/ventoy/plugin/ventoy/theme/theme.txt
install -D -m 755 %{name}-%{version}/plugin/ventoy/ventoy.json %{buildroot}/opt/ventoy/plugin/ventoy/ventoy.json
install -D -m 755 %{name}-%{version}/CreatePersistentImg.sh %{buildroot}/opt/ventoy/CreatePersistentImg.sh
install -D -m 755 %{name}-%{version}/boot/boot.img %{buildroot}/opt/ventoy/boot/boot.img
install -D -m 755 %{name}-%{version}/boot/core.img.xz %{buildroot}/opt/ventoy/boot/core.img.xz

%post
update-desktop-database &> /dev/null

%files
%defattr(-,root,root,-)
/usr/share/applications/ventoy.desktop
/usr/share/icons/ventoy.png
%ifarch x86_64
	/opt/ventoy/tool/x86_64/mount.exfat-fuse.xz
	/opt/ventoy/tool/x86_64/hexdump.xz
	/opt/ventoy/tool/x86_64/ash.xz
	/opt/ventoy/tool/x86_64/Ventoy2Disk.gtk2
	/opt/ventoy/tool/x86_64/V2DServer.xz
	/opt/ventoy/tool/x86_64/vtoycli.xz
	/opt/ventoy/tool/x86_64/Ventoy2Disk.qt5
	/opt/ventoy/tool/x86_64/mkexfatfs.xz
	/opt/ventoy/tool/x86_64/Plugson.xz
	/opt/ventoy/tool/x86_64/Ventoy2Disk.gtk3
	/opt/ventoy/tool/x86_64/vlnk.xz
	/opt/ventoy/tool/x86_64/xzcat
	/opt/ventoy/VentoyGUI.x86_64
%endif
%ifarch aarch64
	/opt/ventoy/tool/aarch64/mount.exfat-fuse.xz
	/opt/ventoy/tool/aarch64/hexdump.xz
	/opt/ventoy/tool/aarch64/ash.xz
	/opt/ventoy/tool/aarch64/V2DServer.xz
	/opt/ventoy/tool/aarch64/vtoycli.xz
	/opt/ventoy/tool/aarch64/Ventoy2Disk.qt5
	/opt/ventoy/tool/aarch64/mkexfatfs.xz
	/opt/ventoy/tool/aarch64/Plugson.xz
	/opt/ventoy/tool/aarch64/Ventoy2Disk.gtk3
	/opt/ventoy/tool/aarch64/vlnk.xz
	/opt/ventoy/tool/aarch64/xzcat
	/opt/ventoy/VentoyGUI.aarch64
%endif
%ifarch mips64el
	/opt/ventoy/tool/mips64el/mount.exfat-fuse.xz
	/opt/ventoy/tool/mips64el/hexdump.xz
	/opt/ventoy/tool/mips64el/ash.xz
	/opt/ventoy/tool/mips64el/V2DServer.xz
	/opt/ventoy/tool/mips64el/vtoycli.xz
	/opt/ventoy/tool/mips64el/Ventoy2Disk.qt5
	/opt/ventoy/tool/mips64el/mkexfatfs.xz
	/opt/ventoy/tool/mips64el/Plugson.xz
	/opt/ventoy/tool/mips64el/Ventoy2Disk.gtk3
	/opt/ventoy/tool/mips64el/vlnk.xz
	/opt/ventoy/tool/mips64el/xzcat
	/opt/ventoy/VentoyGUI.mips64el
%endif
/opt/ventoy/ventoy/ventoy.disk.img.xz
/opt/ventoy/ventoy/version
/opt/ventoy/VentoyVlnk.sh
/opt/ventoy/VentoyWeb.sh
/opt/ventoy/tool/VentoyGTK.glade
/opt/ventoy/tool/ENROLL_THIS_KEY_IN_MOKMANAGER.cer
/opt/ventoy/tool/VentoyWorker.sh
/opt/ventoy/tool/plugson.tar.xz
/opt/ventoy/tool/distro_gui_type.json
/opt/ventoy/tool/ventoy_lib.sh
/opt/ventoy/tool/languages.json
/opt/ventoy/WebUI/static/css/font-awesome.min.css
/opt/ventoy/WebUI/static/css/ionicons.min.css
/opt/ventoy/WebUI/static/css/vtoy.css
/opt/ventoy/WebUI/static/img/VentoyLogo.png
/opt/ventoy/WebUI/static/img/dropdown.png
/opt/ventoy/WebUI/static/img/refresh.ico
/opt/ventoy/WebUI/static/js/vtoy.js
/opt/ventoy/WebUI/static/js/jQuery-2.1.4.min.js
/opt/ventoy/WebUI/static/js/jquery.validate.min.js
/opt/ventoy/WebUI/static/js/jquery.vtoy.alert.js
/opt/ventoy/WebUI/static/js/languages.js
/opt/ventoy/WebUI/static/bootstrap/css/bootstrap.min.css
/opt/ventoy/WebUI/static/bootstrap/css/bootstrap-theme.min.css
/opt/ventoy/WebUI/static/bootstrap/js/bootstrap.min.js
/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.woff2
/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.ttf
/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.eot
/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.woff
/opt/ventoy/WebUI/static/bootstrap/fonts/glyphicons-halflings-regular.svg
/opt/ventoy/WebUI/static/fonts/fontawesome-webfont.ttf
/opt/ventoy/WebUI/static/fonts/fontawesome-webfont.woff2
/opt/ventoy/WebUI/static/fonts/glyphicons-halflings-regular.woff2
/opt/ventoy/WebUI/static/fonts/glyphicons-halflings-regular.ttf
/opt/ventoy/WebUI/static/fonts/ionicons.eot
/opt/ventoy/WebUI/static/fonts/glyphicons-halflings-regular.woff
/opt/ventoy/WebUI/static/fonts/ionicons.ttf
/opt/ventoy/WebUI/static/fonts/fontawesome-webfont.woff
/opt/ventoy/WebUI/static/AdminLTE/css/skins/skin-blue.min.css
/opt/ventoy/WebUI/static/AdminLTE/css/AdminLTE.min.css
/opt/ventoy/WebUI/static/AdminLTE/js/app.min.js
/opt/ventoy/WebUI/favicon.ico
/opt/ventoy/WebUI/index.html
/opt/ventoy/ExtendPersistentImg.sh
/opt/ventoy/Ventoy2Disk.sh
/opt/ventoy/VentoyPlugson.sh
/opt/ventoy/plugin/ventoy/ventoy_grub.cfg
/opt/ventoy/plugin/ventoy/theme/menu_n.png
/opt/ventoy/plugin/ventoy/theme/menu_ne.png
/opt/ventoy/plugin/ventoy/theme/slider_c.png
/opt/ventoy/plugin/ventoy/theme/select_c.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_s.png
/opt/ventoy/plugin/ventoy/theme/slider_s.png
/opt/ventoy/plugin/ventoy/theme/menu_sw.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_c.png
/opt/ventoy/plugin/ventoy/theme/menu_w.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_ne.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_w.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_e.png
/opt/ventoy/plugin/ventoy/theme/slider_n.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_nw.png
/opt/ventoy/plugin/ventoy/theme/menu_e.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_sw.png
/opt/ventoy/plugin/ventoy/theme/menu_c.png
/opt/ventoy/plugin/ventoy/theme/icons/ubuntu.png
/opt/ventoy/plugin/ventoy/theme/icons/deepin.png
/opt/ventoy/plugin/ventoy/theme/icons/vtoyiso.png
/opt/ventoy/plugin/ventoy/theme/icons/red-hat.png
/opt/ventoy/plugin/ventoy/theme/background.png
/opt/ventoy/plugin/ventoy/theme/menu_s.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_n.png
/opt/ventoy/plugin/ventoy/theme/terminal_box_se.png
/opt/ventoy/plugin/ventoy/theme/menu_nw.png
/opt/ventoy/plugin/ventoy/theme/menu_se.png
/opt/ventoy/plugin/ventoy/theme/theme.txt
/opt/ventoy/plugin/ventoy/ventoy.json
/opt/ventoy/CreatePersistentImg.sh
/opt/ventoy/boot/boot.img
/opt/ventoy/boot/core.img.xz
/opt/ventoy/README

%doc
/tmp/ventoy-change.log

%changelog
* Fri Feb 03 2023 Karlis Kavacis karlis.kavacis@protonmail.com
- Added checksum checking

* Wed Feb 01 2023 Karlis Kavacis karlis.kavacis@protonmail.com
- Version change to v1.0.88

* Tue Nov 22 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build
